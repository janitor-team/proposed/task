#!/bin/sh

set -eux
version="$1"
echo downloading git tag v${version}
git clone -b v${version} --depth 1 --recursive https://github.com/GothenburgBitFactory/taskwarrior.git task-git
tar -cJ --wildcards --owner=root --group=root --mode=a+rX --exclude '.git*' --exclude doc/ref -f task_${version}+dfsg.orig.tar.xz task-git
rm -rf task-git
