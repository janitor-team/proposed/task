task (2.5.3+dfsg-4) unstable; urgency=medium

  * d/watch: new github tarball URL
  * Patch an infinite loop for multi-month recurrence. This was previously
    patched in 2.5.1+dfsg-9 but wrongly dropped in 2.5.2+dfsg-1; patch from
    Michał Mirosław. (Closes: #951776, again)

 -- Gordon Ball <gordon@chronitis.net>  Fri, 28 May 2021 12:00:06 +0000

task (2.5.3+dfsg-3) unstable; urgency=medium

  * Backport taskwarrior#2405 which fixes further parser-related regressions.
    (Closes: #980263, again).
  * Fix bash-based tests being skipped in autopkgtest

 -- Gordon Ball <gordon@chronitis.net>  Mon, 08 Feb 2021 19:52:03 +0000

task (2.5.3+dfsg-2) unstable; urgency=medium

  * Backport taskwarrior#2395 to fix a regression where names containing
    hyphens and numbers were interpreted as expressions (Closes: #980263)

 -- Gordon Ball <gordon@chronitis.net>  Mon, 25 Jan 2021 19:24:01 +0000

task (2.5.3+dfsg-1) unstable; urgency=medium

  * Update build dependencies
    - Drop dh-buildinfo (obsolete)
    - Drop libjson-perl (no longer used in tests)
    - Drop libreadline-dev (not used since tasksh was split off)
    - Annotate git and python3 as !nocheck
    Thanks to Helmet Grohne for reporting this (Closes: #979079)
  * New upstream version 2.5.3+dfsg
  * Update team email address to Debian Tasktools Team
    <team+tasktools@tracker.debian.org>

 -- Gordon Ball <gordon@chronitis.net>  Mon, 04 Jan 2021 19:14:58 +0000

task (2.5.2+dfsg-1) unstable; urgency=medium

  * d/watch: check github instead of taskwarrior.org
  * get-orig-source: simplify, update git URL to github
  * New upstream version 2.5.2+dfsg
  * Rebase patches; drop those fixed upstream:
    - 0012-Patch-infinite-loop-with-P2M-recurrence.patch
    - 010_fix-manpage-rule-precedence-color.patch
    - 011_fix_non_ascii_truncation.patch

 -- Gordon Ball <gordon@chronitis.net>  Tue, 15 Dec 2020 10:05:56 +0000

task (2.5.1+dfsg-11) unstable; urgency=medium

  * Drop autopkgtest using (rc-buggy) adequate (Closes: #971831)

 -- Gordon Ball <gordon@chronitis.net>  Thu, 15 Oct 2020 18:07:46 +0000

task (2.5.1+dfsg-10) unstable; urgency=medium

  * Port the testsuite to python3 (Closes: #938626)
  * Use dh_bash-completion to install the bash completion script
  * Use debhelper compatibility level 13
  * Lintian cleanup:
     + Renamed tag typo-in-manual-page in overrides
     + Renamed TODO.Debian -> TODO

 -- Gordon Ball <gordon@chronitis.net>  Wed, 19 Aug 2020 17:49:02 +0000

task (2.5.1+dfsg-9) unstable; urgency=medium

  * Fix infinite loop caused by multimonth recurrence; patch from Michał
    Mirosław. (Closes: #951776)
  * Update to Standards-Version 4.5.0
  * Replace ADTTMP with AUTOPKGTEST_TMP in autopkgtests
  * Set Rules-Requires-Root: no
  * Update all references from "python" to "python2"; ubuntu patch from Balint
    Reczey, version 2.5.1+dfsg-8ubuntu1

 -- Gordon Ball <gordon@chronitis.net>  Tue, 03 Mar 2020 10:42:08 +0000

task (2.5.1+dfsg-8) unstable; urgency=medium

  * Add a patch which fixes truncated annotations when they contain non-ASCII
    characters (with thanks to Apollon; closes: #820108)
  * Update to Standards-Version 4.4.0
  * Use debhelper compatibility level 12

 -- Gordon Ball <gordon@chronitis.net>  Thu, 26 Sep 2019 08:48:36 +0000

task (2.5.1+dfsg-7) unstable; urgency=medium

  * Update VCS URLs to salsa.debian.org
  * Update to Standards-Version 4.3.0
  * Stop calling dpkg-parsechangelog in d/rules, and use pkg-info.mk instead
  * Remove debian/source/options (forcing xz compression), use dpkg defaults

 -- Gordon Ball <gordon@chronitis.net>  Fri, 22 Feb 2019 19:05:59 +0000

task (2.5.1+dfsg-6) unstable; urgency=medium

  * Patch shebang lines in python scripts from "/usr/bin/env python"
    to "/usr/bin/python" (prompted by LP:1663628)

 -- Gordon Ball <gordon@chronitis.net>  Tue, 21 Nov 2017 10:32:28 +0000

task (2.5.1+dfsg-5) unstable; urgency=medium

  * Correct install path for bash-completion (closes: #880539)

 -- Gordon Ball <gordon@chronitis.net>  Thu, 02 Nov 2017 09:43:33 +0000

task (2.5.1+dfsg-4) unstable; urgency=medium

  * Retire transition package "task" (closes: #878982)
  * Remove "task" breaks/replaces from "taskwarrior"
  * Update Standards-Version to 4.1.1, no changes required
  * Update debhelper compat level to 10
  * Use https URLs in d/copyright and d/watch
  * Fix d/rules and d/taskwarrior.{install,manpages} for changes
    resulting from now building only a single binary package

 -- Gordon Ball <gordon@chronitis.net>  Mon, 23 Oct 2017 19:49:27 +0000

task (2.5.1+dfsg-3) unstable; urgency=medium

  * Move zsh completer to /usr/share/zsh/vendor-completions (closes: #843576)

 -- Gordon Ball <gordon@chronitis.net>  Thu, 23 Feb 2017 20:32:01 +0100

task (2.5.1+dfsg-2) unstable; urgency=medium

  [ Gordon Ball ]
  * Fix python dependency for tests
  * Use the installed binary for tests

  [ Sebastien Badia ]
  * Updated SV to 3.9.8, no changes required
  * Set maintainer to tasktools team
  * Update my email to the Debian one

 -- Gordon Ball <gordon@chronitis.net>  Tue, 13 Dec 2016 15:40:01 +0100

task (2.5.1+dfsg-1) unstable; urgency=medium

  [ Gordon Ball ]
  * Add a patch for build-time tests which fail depending on the date
  * Update autopkgtests; remove the adequate test for the
    transitional `task` package, and fix the script running
    the package's unit tests.
  * Updated SV to 3.9.7, no changes required.

  [ Sebastien Badia ]
  * New upstream release: 2.5.1
  * d/control: Switch to https for Vcs-Git and homepage
  * Update dates in d/copyright
  * d/patches: Refresh patches
  * d/rules:
      Fix TASK_RCDIR, fix manpage rc location (Closes: #789424)
      Fix TASK_RCDIR, fix theme location (Closes: #818103)
      Enable hardening builds flags
  * Fix spurious "pri." in taskrc manpage (Closes: #789423)
  * Add lintian overrides "isnt" is a keyword of taskwarrior

 -- Sebastien Badia <seb@sebian.fr>  Sat, 02 Apr 2016 20:32:52 +0200

task (2.5.0+dfsg-1) unstable; urgency=medium

  * New upstream release: 2.5.0

 -- Sebastien Badia <seb@sebian.fr>  Wed, 21 Oct 2015 23:22:03 +0200

task (2.4.4+dfsg-1) unstable; urgency=medium

  [ Gordon Ball ]
  * New upstream release: 2.4.4
  * Bash and fish completions now installed under /usr/share (see #776954)
  * Build should now be reproducible (`task diagnostics` will no longer
    show the build timestamp, which shouldn't make too much difference)

  [ Sebastien Badia ]
  * Upstream fix for typo in the 'NEWS' file (Closes: #789417)
  * Maintain a NEWS file with not only the latest version (Closes: #789420)

 -- Sebastien Badia <seb@sebian.fr>  Sun, 12 Jul 2015 11:45:10 +0200

task (2.4.3+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

 -- Sebastien Badia <seb@sebian.fr>  Wed, 29 Apr 2015 01:38:54 +0200

task (2.4.3+dfsg-1) experimental; urgency=medium

  [ Gordon Ball ]
  * New upstream release: 2.4.3
  * d/patches: Refresh for new release.

  [ Sebastien Badia ]
  * Fix version number in the maint script; Thanks Jakub! (Closes: #710321)

 -- Sebastien Badia <seb@sebian.fr>  Tue, 21 Apr 2015 17:59:33 +0200

task (2.4.1+dfsg-3) unstable; urgency=medium

  * Re-introduce Jakub fix for bash completion file (closes: #710321):
    + Rename /etc/bash_completion.d/task.sh back as /etc/bash_completion.d/task
    + Update debian/rules accordingly.
    + Add debian/maintscript to let dh_installdeb add maintainer script
      snippets to remove the old file, Debian version 2.4.1-1 was accidentally
      uploaded to unstable.
  * Fix install step (re-add rc files) (Closes: #779914).

 -- Sebastien Badia <seb@sebian.fr>  Sat, 07 Mar 2015 23:27:50 +0100

task (2.4.1+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

 -- Sebastien Badia <seb@sebian.fr>  Fri, 06 Mar 2015 07:04:47 +0100

task (2.4.1+dfsg-1) unstable; urgency=medium

  [ Gordon Ball ]
  * d/patchs: Refresh according new upstream release
    * tests-fail.diff (fixed upstream, TW-1296)
    * tests-for-released-version.diff (fixed upstream)
    * tests-count-eom.diff (fixed upstream, TW-1295)
    * fix-task-sync-man-url.diff (fixed upstream)
  * Enable GnuTLS support (closes: #741625, LP: #1344560)
  * tasksh is now a separate project and no longer included in this package

  [ Sebastien Badia ]
  * New upstream release: 2.4.1 (LP: #1286939)
  * New maintainer (co-maint. with Gordon) (closes: #762130)
    * Many thanks to Gordon, Michel, Jack L. and Tobias Frost for the help!
  * d/NEWS: Add a note about the removal of tasksh.
  * d/copyright: Convert to dep5 format and fix lintian warnings.
  * d/rules: Convert to shorted dh format.
  * d/patchs: Sort, refresh, and remove unsed patchs.
    * Add missing DEP-3 headers on d/p/no-task-ref.patch
  * Enable full taskwarrior test suite (including python tests)
    (Thanks to Michel Voßkuhle)
  * Refresh debian/get-orig-source.sh (this script is used to import
    taskwarrior test-suite in the orig tarball)
  * d/control: Many changes
    * Update standard-version (no changes)
    * Upgrade debhelper compat format to 9
    * Remove dpkg-dev build dependency
    * Add python and git to build-deps (for the test suite)
    * Add vcs-* fields (this package is now managed with GIT)
  * Get package lintian clean
    * remove un-needed lintian override
    * fix quilt-patch-missing-description
    * fix manpage-has-errors-from-man
    * fix hyphen-used-as-minus-sign

 -- Sebastien Badia <seb@sebian.fr>  Sun, 01 Mar 2015 22:03:25 +0100

task (2.3.0+dfsg-3) unstable; urgency=low

  * Improve the get-orig-source script:
    + Use the POSIX 1003.1-1988 (ustar) format for tarballs.
  * Update the watch file.
  * Add patch (default-editor.diff) to change the default editor from “vi” to
    “editor”, as per Policy §11.4.
  * Add patch (fix-task-sync-man-url.diff) to fix task server setup
    documentation URL (closes: #749180).
    Thanks to Axel Beckert for the bug report.
  * Use “dpkg-parsechangelog -SVersion” for extracting version number in
    debian/rules.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 31 May 2014 23:22:30 +0200

task (2.3.0+dfsg-2) unstable; urgency=low

  * Update git URL in the get-orig-source script.
  * Use dh-buildinfo:
    + Update debian/rules.
    + Add the package to Build-Depends.
  * Add DEP-8 tests that run adequate(1) against the packages.
  * Add patch (tests-count-eom.diff) to make the count.t test failures on the
    last day of the month non-fatal (closes: #743238). Thanks to James
    Dietrich for the bug report and the analysis of the problem.
  * Remove a long obsolete comment from debian/rules.
  * Drop tests-unset-env-vars.diff, as the problem of test suite stomping on
    user's data has been fixed upstream.
    Set TASKDATA and TASKRC to specially-crafted values (both in debian/rules
    and in the DEP-8 test), so that task trips over it if the problem
    reappears in the future
  * Mark tests-fail.diff as forwarded upstream.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 02 Apr 2014 11:33:52 +0200

task (2.3.0+dfsg-1) unstable; urgency=low

  * Upload to unstable.
  * Repackage .orig.tar to remove documentation that cannot be edited with
    free tools (closes: #737478).
  * Add patch (no-task-ref.diff) not try to install task-ref.pdf, as it's now
    missing.
  * Drop code to remove pre-built static libraries from debian/rules, as they
    are not included in the repacked .orig.tar.
  * Switch to xz compression, both for .orig.tar and .debian.tar.
  * Update the watch file.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 13 Feb 2014 23:33:28 +0100

task (2.3.0-1) experimental; urgency=low

  * New upstream release.
  * Fix debian/watch to correctly handle non-beta versions.
  * Drop patches that were applied upstream:
    + customizable-rcdir.diff;
    + no-path-max.diff;
    + tests-fix-pri-sort.diff;
    + tests-fix-bug.annual.diff;
    + system-kfreebsd.diff;
    + system-hurd.diff;
    + typos.diff.
  * Refresh remaining patches.
  * Add patch (fix-groff-warnings.diff) to fix some groff warnings.
  * Force gzip for .debian.tar compression.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 19 Jan 2014 23:19:34 +0100

task (2.3.0~beta2-2) experimental; urgency=low

  * Add patches (tests-fix-pri-sort.diff, tests-fix-bug.annual.diff) to fix a
    few test failures.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 08 Jan 2014 12:23:45 +0100

task (2.3.0~beta2-1) experimental; urgency=low

  * New upstream beta release.
  * Update the watch file and the get-orig-source script to accept beta
    versions.
  * Drop patches that were applied upstream:
    + tests-out-of-tree.diff;
    + tests-temporary-directory.diff;
    + directory-no-path-max.diff;
    * directory-handle-dt-unknown.diff.
  * Refresh remaining patches.
  * Fix support for GNU/Hurd and GNU/kFreeBSD:
    + Update system-kfreebsd.diff; partially applied upstream.
    + Update system-hurd.diff; partially applied upstream.
    + Add patch (no-path-max.diff) to use get_current_dir_name() instead of
      relying on PATH_MAX, as the latter is not defined on GNU/Hurd.
  * Add code to remove pre-built static libraries to debian/rules; the static
    libraries are back in .orig.tar…
  * Adapt the test suite to Debian needs:
    + Update tests-for-released-version.diff to allow running tests against a
      beta version of taskwarrior.
    + Add patch (tests-time-independent.diff) to disable checking if copyright
      information covers the current year.
  * Build-depend on libreadline-dev (needed by the new tasksh command).
  * Link with --as-needed, to avoid direct dependencies on ncurses.
  * Update DEP-8 tests for tasksh.
  * Update Depends in the DEP-8 control file, to takie into account the binary
    package rename.
  * Drop tests-set-build-type.diff. Instead, add a patch
    (enable-warnings.diff) to enable warnings globally and to stop hardcoding
    build type. Set build type in debian/rules via cmake configuration
    variable.
  * Add patch (typos.diff) to fix a few spelling mistakes.
  * Disable GnuTLS support for the time being.

 -- Jakub Wilk <jwilk@debian.org>  Tue, 07 Jan 2014 22:35:48 +0100

task (2.2.0-4) experimental; urgency=low

  * Fix a typo in the copyright file.
  * Update years in the copyright file.
  * Bump standards version to 3.9.5 (no changes needed).
  * Replace patch to hardcode Debian-specific paths (debian-paths.diff) with a
    cleaner patch (customizable-rcdir.diff) that makes TASK_RCDIR customizable
    at configure time. Update deban/rules to set TASK_DOCDIR and TASK_RCDIR at
    configure time.
  * Rename the binary package: task → taskwarrior (closes: #719317).
    Thanks to Ben Armstrong for the bug report.
    + Rename the main package as “taskwarrior” in debian/control; add
      Conflicts/Replaces against the old “task” package.
    + Add transitional package to debian/control.
    + Move Pre-Depends to the transitional package.
    + Rename debhelper configuration files.
    + Update debian/rules and README.Debian accordingly.
    + Make /usr/share/task a symlink to /usr/share/taskwarrior, for backwards
      compatibility.
    + Add postinst script to deal with transitioning /usr/share/task from
      directory to symlink.
    + Add lintian override for conflicts-with-version. Conflicts makes
      transitioning /usr/share/task from directory to symlink slightly easier.
  * Keep the i18n directory in /usr/share/doc.
  * Drop code to remove pre-built static libraries from debian/rules; the
    static libraries are no longer shipped in .orig.tar.
  * Ship add-ons in /usr/share/doc as examples.
  * Stop dh_compress from compressing examples.
  * Refresh patches.
  * Add “XS-Testsuite: autopkgtest”.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 04 Jan 2014 19:56:06 +0100

task (2.2.0-3) unstable; urgency=low

  * Rename /etc/bash_completion.d/task.sh back as /etc/bash_completion.d/task
    (closes: #710321):
    + Update debian/rules accordingly.
    + Add debian/maintscript to let dh_installdeb add maintainer script
      snippets to remove the old file.
    + Bump minimum required version of debhelper to 8.1 (for
      dpkg-maintscript-helper support.)
    + Add ${misc:Pre-Depends} to Pre-Depends. (It's not strictly necessary for
      unstable, but it eases wheezy backports).
  * Add DEP-8 tests.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 03 Jun 2013 18:47:50 +0200

task (2.2.0-2) unstable; urgency=low

  * Upload to unstable.
  * Adapt the test suite to Debian needs:
    + Add patch (tests-set-build-type.diff) to set cmake build type to ‘debug’
      and append -Wall to CXXFLAGS. That way tests are built with the same
      flags as normal code.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 12 May 2013 02:12:45 +0200

task (2.2.0-1) experimental; urgency=low

  * New upstream release.
    + This release no longer supports Lua extensions.
      - Remove lublua5.1-dev from Build-Depends.
    + Drop fix-manpage-formatting.diff; applied upstream.
    + Drop fix-manpage-installation.diff; applied upstream.
    + Drop fix-typos.diff; applied upstream.
    + Refresh other patches.
    + Update debian/copyright.
    + Fix duplication of completed task on merge (closes: #700255).
      Thanks to Helmut Grohne for the bug report.
  * Add supplemental .orig.tar with the test suite.
    + Add get-orig-source target, which imports the tests from the upstream
      VCS.
  * Adapt the test suite to Debian needs:
    + Add patch (tests-out-of-tree.diff) to allow running tests out-of-tree
    + Add patch (tests-unset-env-vars.diff) unset TASK* environment variables.
    + Add patch (tests-fail.diff) to make the test runner exit with non-zero
      code if any of the tests fail.
    + Add patch (tests-for-released-version.diff) to allow running tests
      against a released version of taskwarrior.
    + Add patch (tests-temporary-directory.diff) to fix insecure use of /tmp.
  * Run tests at build time.
    + Update debian/rules.
    + Add libjson-perl to Build-Depends.
  * Add patch (directory-no-path-max.diff) to use get_current_dir_name()
    instead of relying on PATH_MAX, as the latter is not defined on GNU/Hurd.
  * Add patch (directory-handle-dt-unknown.diff) to properly handle d_type ==
    DT_UNKNOWN.
  * Add patch (system-hurd.diff) to add support for GNU/Hurd systems.
  * Add patch (system-kfreebsd.diff) to add support for GNU/kFreeBSD systems.
  * Link against libuuid.
    + Add uuid-dev to Build-Depends.
  * Don't use dh_testdir; instead use target dependencies to ensure that
    debian/rules is run from the correct directory.
  * Run dh_clean as the first command in the clean target.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 11 Apr 2013 00:43:17 +0200

task (2.1.2-1) experimental; urgency=low

  * New maintainer.
    Thanks to Alexander Neumann for his past work on this package.
  * Relicense Debian packaging to the same license upstream uses (MIT), as
    agreed with Alexander.
  * New upstream release (closes: 693979).
    + Fix order of priorities in the project summary (closes: #680353).
      Thanks to Marcin Kulisz for the bug report.
  * Update debian/copyright.
  * Bump standards version to 3.9.4 (no changes needed).
  * Switch to source format 3.0 (quilt).
  * Remove pre-built static libraries early in the build* targets.
  * Remove cmake.h in the clean target.
  * Rewrite debian/rules from scratch, without cdbs.
    + Drop "cbds" from Build-Depends.
    + Add "dpkg-dev (>= 1.15.7)" to Build-Depends, for dpkg-buildflags.
  * Don't install vim support files to the binary package; they are already
    included in the vim package itself.
  * Don't install /usr/share/task/refresh to the binary package; it's not
    useful at runtime.
  * Fix TASK_RCDIR in cmake.h (closes: #669344).
    Thanks to Sylvain Fankhauser for the bug report.
  * Add Lintian override for spelling-error-in-manpage; the misspelling in
    question is deliberate.
  * Add patch (fix-manpage-formatting.diff) to fix a formatting error in the
    task(1) manual page (closes: #699332).
  * Add patch (fix-manpage-installation.diff) to fix manual page installation
    for out-of-tree builds.
  * Add patch (fix-typos.diff) to fix some spelling errors.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 27 Feb 2013 17:05:06 +0100

task (2.0.0-1) unstable; urgency=low

  * new upstream version (closes: #666903)
  * this version can be built using gcc-4.7 (closes: #667390)

 -- Alexander Neumann <alexander@debian.org>  Sun, 08 Apr 2012 12:37:11 +0200

task (1.9.4-2) unstable; urgency=low

  * include right path to colorthemes in generated .taskrc (closes: #638269)

 -- Alexander Neumann <alexander@debian.org>  Thu, 25 Aug 2011 22:18:17 +0200

task (1.9.4-1) unstable; urgency=low

  * new upstream version (closes: #635141)
  * update standards version to 3.9.2 (no changes)

 -- Alexander Neumann <alexander@debian.org>  Mon, 01 Aug 2011 22:01:52 +0200

task (1.9.3-1) unstable; urgency=low

  * new upstream version
  * update standards version to 3.9.1 (no changes)

 -- Alexander Neumann <alexander@debian.org>  Sun, 05 Dec 2010 13:46:37 +0100

task (1.9.2-1) unstable; urgency=low

  * new upstream version
  * update standards version to 3.9.0

 -- Alexander Neumann <alexander@debian.org>  Tue, 20 Jul 2010 10:02:25 +0200

task (1.9.1-1) unstable; urgency=low

  * new upstream version

 -- Alexander Neumann <alexander@debian.org>  Mon, 31 May 2010 16:55:14 +0200

task (1.9.0-3) unstable; urgency=low

  * restored old (buggy) behaviour, as libuuid is untested (by upstream)
    (reopens #573066, will be fixed upstream in 1.9.1)

 -- Alexander Neumann <alexander@debian.org>  Tue, 09 Mar 2010 12:32:30 +0100

task (1.9.0-2) unstable; urgency=low

  * link against libuuid, use library function for generating uuids instead
    of internal (not rfc-conforming) implementation (closes: #573066)

 -- Alexander Neumann <alexander@debian.org>  Mon, 08 Mar 2010 22:57:19 +0100

task (1.9.0-1) unstable; urgency=low

  * new upstream version
  * update standards version to 3.8.4
  * change copyright notice for scripts/*, Thanks Torsten!

 -- Alexander Neumann <alexander@debian.org>  Tue, 23 Feb 2010 01:17:40 +0100

task (1.8.5-2) unstable; urgency=low

  * Add libncurses5-dev to Build-Deps

 -- Alexander Neumann <alexander@debian.org>  Tue, 09 Feb 2010 22:13:58 +0100

task (1.8.5-1) unstable; urgency=low

  * Initial release (closes: #531587)

 -- Alexander Neumann <alexander@debian.org>  Tue, 02 Feb 2010 15:33:14 +0100
